const config = require('./config');
const mysql = require('mysql');

const fromSql = mysql.createConnection({
    host: config.sql.from.host,
    user: config.sql.from.user,
    password: config.sql.from.pass,
    database: config.sql.from.database,
    multipleStatements: true,
    typeCast: true
});

const toSql = mysql.createConnection({
    host: config.sql.to.host,
    user: config.sql.to.user,
    password: config.sql.to.pass,
    database: config.sql.to.database,
    multipleStatements: true,
    typeCast: true
});

module.exports = {
    mysql,
    fromSql,
    toSql
};
