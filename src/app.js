const sql = require('./sql');

const getOldUsers = () => {
    return new Promise(
        (resolve, reject) => {
            sql.fromSql.query('SELECT id, firstname AS firstName, lastname AS lastName, email AS email, student_id AS studentNumber, phone AS phone, promotion AS promotionId, repartition_group AS `group` FROM medecin1_stages.`user` WHERE promotion IN (\'DFASM1\', \'DFASM2\', \'DFASM3\')',
                [],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject();
                    } else {
                        result.forEach(
                            (user) => {
                                switch (user.promotionId) {
                                    case 'DFASM1' :
                                        user.promotionId = 2;
                                        break;
                                    case 'DFASM2' :
                                        user.promotionId = 3;
                                        break;
                                    case 'DFASM3':
                                        user.promotionId = 4;
                                        break;
                                }

                                switch (user.group) {
                                    case 'A' :
                                        user.group = 2;
                                        break;
                                    case 'B' :
                                        user.group = 3;
                                        break;
                                    default:
                                        user.group = 1;
                                        break;
                                }
                            }
                        );
                        resolve(result);
                    }
                });
        }
    );
};

const main =
    async () => {
        const users = await getOldUsers();

        users.forEach(
            (user) => {
                sql.toSql.query('INSERT INTO tirajosaure_dev.users (firstName, lastName, email, password, studentNumber, phone, promotionId, `group`) VALUES (?, ?, ?, \'\', ?, ?, ?, ?)',
                    [user.firstName, user.lastName, user.email, user.studentNumber, user.phone, user.promotionId, user.group],
                    (error) => {
                        if (error) {
                            console.error(error);
                        } else {
                            console.log('Insert ' + user.id);
                        }
                    });
            }
        );

    };

main()
    .then(() => {console.log('Success')})
    .catch((error) => console.error(error));
